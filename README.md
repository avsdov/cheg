# CHEΓ

Cultural Heritage Export Gadget

The Russian Wikivoyage hosts the largest database of cultural heritage monuments in Russia.
It contains over 180 thousand objects, but only slightly less than 10,000 of them have a representation in the Wikidata.
Volunteers of the Wikivoyage keep working on the completeness, accuracy and consistency of descriptions of cultural heritage objects.
This tool implements consistent uploading of basic information about the specified cultural heritage object to the Wikidata from the Wikivoyage.

The following information is exported for the specified object from the Wikivoyage database to to Wikidata:

* Object name in Russian (name field) - entity label.
* Image (image field) - statement for property P18 (image).
* Object identifier (knid field) - statement for property P1483 (kulturnoe-nasledie.ru ID).
* If the object is part of a complex (field complex) - statement for property P361 (part of).
* Number from the state register (EGROKN) (knid-new field) - statement for property P5381 (EGROKN ID).
* Object type (field type) - statement for property P31 (instance of): for a monument of urban planning and architecture - Q2319498 (landmark), for a historical monument - Q1081138 (historic site), for an object of monumental art - Q4989906 (monument), for a monument archeology - Q839954 (archeological site), for a historical settlement (settlement) - Q3920245 (historical city in Russia).
* A statement is generated for property P1434 (heritage designation) with the value Q8346700 (cultural heritage site in Russia) and references based on information about the object's state protection.
* Locality (munid field) - statement for property P131 (located in ATE).
* An unstructured address (address field) is translated into a statement for property P2795 (directions).
* Coordinates (lat and long fields) - statement for the P625 (coordinate location) property.
* Russian Wikipedia page (wiki field) - ruwiki link associated with this entity.
* Wikimedia Commons Category (commonscat field) - the associated commonswiki link.

This tool is hosted on Wikimedia Toolforge.
