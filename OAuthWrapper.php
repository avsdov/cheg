<?php

/**
 * Replacement of Guzzle HTTP-client for OAuth support in MediaWiki API requests
 *
 * @author Avsolov
 *
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 */


use \GuzzleHttp\ClientInterface;

use \GuzzleHttp\Exception\GuzzleException;
use \GuzzleHttp\Promise\PromiseInterface;
use \Psr\Http\Message\RequestInterface;
use \Psr\Http\Message\ResponseInterface;
use \Psr\Http\Message\StreamInterface;
use \Psr\Http\Message\UriInterface;
use \MediaWiki\OAuthClient\Client;
use \MediaWiki\OAuthClient\Token;

class OAuthWrapper implements ClientInterface {
    private $accessToken = null;
    private $oclient = null;

    /**
     * @param string $apiUrl The API Url
     */
    public function __construct(Token $at, Client $oclient) {
        if (empty($at) or empty($oclient)) throw new Exception('Illegal access token');
        $this->accessToken = $at;
        $this->oclient = $oclient;
    }

    /**
     * Send an HTTP request.
     *
     * @param RequestInterface $request Request to send
     * @param array            $options Request options to apply to the given
     *                                  request and to the transfer.
     *
     * @throws GuzzleException
     */
    public function send(RequestInterface $request, array $options = []): ResponseInterface {
        throw new Exception('Not implemented');
    }

    /**
     * Asynchronously send an HTTP request.
     *
     * @param RequestInterface $request Request to send
     * @param array            $options Request options to apply to the given
     *                                  request and to the transfer.
     */
    public function sendAsync(RequestInterface $request, array $options = []): PromiseInterface {
        throw new Exception('Not implemented');
    }

    /**
     * Create and send an HTTP request.
     *
     * Use an absolute path to override the base path of the client, or a
     * relative path to append to the base path of the client. The URL can
     * contain the query string as well.
     *
     * @param string              $method  HTTP method.
     * @param string|UriInterface $uri     URI object or string.
     * @param array               $options Request options to apply.
     *
     * @throws GuzzleException
     */
    public function request(string $method, $uri, array $options = []): ResponseInterface {
        $isPost = false;
        switch ($method) {
            case 'GET':  $isPost = false; break;
            case 'POST': $isPost = true; break;
            default: throw new Exception('Not implemented');
        }
        $url = $uri;
        if (isset($options['query'])) $url .= '?'.http_build_query($options['query']);
        $body = $this->oclient->makeOAuthCall($this->accessToken, $url, $isPost, isset($options['form_params']) ? $options['form_params'] : null);
        return new class($body) implements ResponseInterface {
            private $body = null;
            public function __construct($body) { $this->body = $body; }
            public function getBody() { return $this->body; }
            public function getStatusCode() { throw new Exception('Not implemented'); }
            public function withStatus($code, $reasonPhrase = '') { throw new Exception('Not implemented'); }
            public function getReasonPhrase() { throw new Exception('Not implemented'); }
            public function getProtocolVersion() { throw new Exception('Not implemented'); }
            public function withProtocolVersion($version) { throw new Exception('Not implemented'); }
            public function getHeaders() { throw new Exception('Not implemented'); }
            public function hasHeader($name) { throw new Exception('Not implemented'); }
            public function getHeader($name) { throw new Exception('Not implemented'); }
            public function getHeaderLine($name) { throw new Exception('Not implemented'); }
            public function withHeader($name, $value) { throw new Exception('Not implemented'); }
            public function withAddedHeader($name, $value) { throw new Exception('Not implemented'); }
            public function withoutHeader($name) { throw new Exception('Not implemented'); }
            public function withBody(StreamInterface $body) { throw new Exception('Not implemented'); }
        };
    }

    /**
     * Create and send an asynchronous HTTP request.
     *
     * Use an absolute path to override the base path of the client, or a
     * relative path to append to the base path of the client. The URL can
     * contain the query string as well. Use an array to provide a URL
     * template and additional variables to use in the URL template expansion.
     *
     * @param string              $method  HTTP method
     * @param string|UriInterface $uri     URI object or string.
     * @param array               $options Request options to apply.
     */
    public function requestAsync(string $method, $uri, array $options = []): PromiseInterface {
        throw new Exception('Not implemented');
    }

    /**
     * Get a client configuration option.
     *
     * These options include default request options of the client, a "handler"
     * (if utilized by the concrete client), and a "base_uri" if utilized by
     * the concrete client.
     *
     * @param string|null $option The config option to retrieve.
     *
     * @return mixed
     *
     * @deprecated ClientInterface::getConfig will be removed in guzzlehttp/guzzle:8.0.
     */
    public function getConfig(?string $option = null) {
    }

}
