<?php

/**
 * This script updates wikidata entity of specified cultural heritage monument in Russia
 * based on information from the Russian Wikivoyage database.
 *
 * @author Avsolov
 *
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 */

require_once('../../lib/vendor/autoload.php');
require_once('./OAuthWrapper.php');
$CRED = parse_ini_file('../../data_checks/cheg.ini');
$AVSBOT = parse_ini_file('../../data_checks/avsbot1.ini');
$ts_pw = posix_getpwuid(posix_getuid());
$ts_mycnf = parse_ini_file($ts_pw['dir'] . "/replica.my.cnf");

/* List of regions of Russia: https://ru_monuments.toolforge.org/regions.txt */
$regs = array();
foreach (file('../regions.txt') as $r) {
    list($rid, $val) = explode('=', $r);
    $regs[trim($rid)] = trim($val);
}

use \Mediawiki\DataModel\EditInfo;
use \Wikibase\DataModel\Reference;
use \Wikibase\DataModel\ReferenceList;
use \Wikibase\DataModel\SiteLink;
use \Wikibase\DataModel\SiteLinkList;
use \Wikibase\DataModel\Entity\Item;
use \Wikibase\DataModel\Entity\ItemId;
use \Wikibase\DataModel\Entity\PropertyId;
use \Wikibase\DataModel\Entity\EntityIdValue;
use \Wikibase\DataModel\Snak\PropertyValueSnak;
use \Wikibase\DataModel\Snak\PropertySomeValueSnak;
use \Wikibase\DataModel\Snak\SnakList;
use \Wikibase\DataModel\Statement\Statement;
use \Wikibase\DataModel\Statement\StatementList;
use \DataValues\UnDeserializableValue;
use \DataValues\StringValue;

require_once('./wikidata_helper.php');

session_start(['cookie_lifetime'=>90000]);
if (empty($_SESSION['CSRF'])) $_SESSION['CSRF'] = sha1(rand().time().session_id());

$row = array();
$wd = false;
$wd_item = false;
set_exception_handler(function ($ex) {
    echo "Увы! SQL-сервер прилёг отдохнуть. Повторите запрос позже...";
    die;
});
$dbh = new PDO('mysql:host=tools-db;dbname=s53506__ruheritage', $ts_mycnf['user'], $ts_mycnf['password']);
unset($ts_mycnf, $ts_pw);
restore_exception_handler();

$oconf = new \MediaWiki\OAuthClient\ClientConfig(URL_WDO.'w/index.php?title=Special:OAuth');
$oconf->setConsumer(new \MediaWiki\OAuthClient\Consumer($CRED['TOKEN'], $CRED['SECRET']));
$oclient = new \MediaWiki\OAuthClient\Client($oconf);

if (isset($_SESSION['requestToken']) && isset($_GET['oauth_verifier'])) {
    $_SESSION['accessToken'] = $oclient->complete($_SESSION['requestToken'], $_GET['oauth_verifier']);
}

if (isset($_SESSION['storedRequest'])) {
    $_REQUEST = $_SESSION['storedRequest'];
    unset($_SESSION['storedRequest']);
}

$ident = null;
if (isset($_SESSION['accessToken'])) {
    $ident = $oclient->identify($_SESSION['accessToken']);
}

$api = new \Mediawiki\Api\MediawikiApi(URL_WDO.'w/api.php',
   is_object($ident) && isset($_REQUEST['action']) ? new OAuthWrapper($_SESSION['accessToken'], $oclient) : null);
$wbFactory = get_wikidata_factory($api);
$termLookup = $wbFactory->newTermLookup();
$propLookup = $wbFactory->newPropertyLookup();

$protection = array(
    '4' => 'Не охраняется государством',
    'В' => 'Выявленный объект',
    'Ф' => 'Объект федерального значения',
    'Р' => 'Объект регионального значения',
    'М' => 'Объект местного значения');

/* Cultural heritage identifier in Wikivoyage */
$id=@sprintf("%010d", $_REQUEST['id']);

do {
    if (empty($id)) break;
    $wv = load_monument($id, $row);
    if (empty($wv->wdid) && @$_REQUEST['action'] != 'create_wd_entity') break;
    if (!isset($_REQUEST['action'])) break;

    /* Non-interface requests */
    try {
        if (@$_REQUEST['csrf'] != @$_SESSION['CSRF']) throw new Exception('Infernal error');

        if (!isset($_SESSION['requestToken'])) {
            $_SESSION['storedRequest'] = $_REQUEST;
            list($authUrl, $_SESSION['requestToken']) = $oclient->initiate();
            header("Location: $authUrl");
            exit();
        }
        if (!isset($_SESSION['accessToken'])) {
            unset($_SESSION['requestToken']);
            unset($_SESSION['storedRequest']);
            throw new Exception('Authorization failed!');
        }

        if (!empty($wv->wdid)) {
            $revision = $wbFactory->newRevisionGetter()->getFromId($wv->wdid);
            $wd_item = $revision->getContent()->getData();
            if (is_object($wd_item)) $wd = $wd_item->getStatements();
        }
        $edit_info = array();
        $refSnaks = array();
        $refSnaks[] = new PropertyValueSnak($P_IMPORTED_FROM, new EntityIdValue(new ItemId(Q_RUS_WIKIVOYAGE)));
        if (!empty($row['revid']))
            $refSnaks[] = new PropertyValueSnak($P_WM_URL, new StringValue(URL_RWV.'?oldid='.$row['revid'].'#'.$id));
        $knid_ref = array();
        if (!empty($row['pwd'])) {
            $knid_ref[] = new PropertyValueSnak($P_STATED_IN, new EntityIdValue(new ItemId('Q'.$row['pwd'])));
        }
        $wv_year = @($wv->year+0);

        if ($_REQUEST['action'] == 'export_label') {
            /* Replace label in Russian (or set a new label) */
            $wd_item->setLabel('ru', $wv->name);
            $wbFactory->newRevisionSaver()->save($revision, new EditInfo('set label[ru]: '.$wv->name));
        }

        if ($_REQUEST['action'] == 'export_image') {
            /* Add new statement P18 with image. If statement exists: warn user, skip action */
            if (empty($wv->image)) throw new Exception('Изображение не задано');
            $wv->image = str_replace('_', ' ', $wv->image);
            $p18 = $wd->getByPropertyId($P_IMAGE);
            if (count($p18->toArray())>1) throw new Exception('Неоднозначность — утверждение для P18 уже есть: заменить? добавить? исправить? ...');
            if (!$p18->isEmpty()) foreach ($p18 as $stmt) {
                $wd->removeStatementsWithGuid($stmt->getGuid());
            }
            $wd->addNewStatement(
                new PropertyValueSnak($P_IMAGE, new StringValue($wv->image)),
                null,
                array(new Reference($refSnaks)));
            $wbFactory->newRevisionSaver()->save($revision, new EditInfo('set [[d:Property:P18|image (P18)]]: '.$wv->image));
        }

        if ($_REQUEST['action'] == 'export_knid') {
            /* Add new statement P1483/P2186/P2817 with Wikivoyage cultural heritage identifier (KNID). If statement exists: replace it */
            // P17 must exist also
            check_and_add_country_statement($id, $wd, $refSnaks, $wv_year);
            set_or_replace_value($wd, new StringValue($wv->knid), $P_KNID,
                empty($row['pwd']) ? null : array(new Reference($knid_ref)));
            if (!empty($row['pwd'])) {
                set_or_replace_value($wd, new EntityIdValue(new ItemId('Q'.$row['pwd'])), $P_LIST, null);
            }
            $p2186 = $wd->getByPropertyId($P_WLM_ID);
            if (!$p2186->isEmpty()) foreach ($p2186 as $stmt) {
                $wd->removeStatementsWithGuid($stmt->getGuid());
            }
            $qua = null;
            if ((strncmp($id,"82",2)==0) || (strncmp($id,"92",2)==0)) {
                if (property_exists($wv, 'uid') and !empty($wv->uid)) {
                    $wd->addNewStatement(
                        new PropertyValueSnak($P_WLM_ID, new StringValue('UA-'.$wv->uid)),
                        new SnakList(array(new PropertyValueSnak($P_OF, $Q_UKRAINE))),
                        null);
                    $edit_info[] = "[[:d:Property:$P_WLM_ID|".get_wd_property($P_WLM_ID.'')." ($P_WLM_ID)]]: {$wv->uid}";
                }
                $qua = new SnakList(array(new PropertyValueSnak($P_OF, $Q_RUSSIA)));
            }
            $wlmid_refs = array(new PropertyValueSnak($P_REF_URL, new StringValue(URL_TFO.$id)));
            $wd->addNewStatement(new PropertyValueSnak($P_WLM_ID, new StringValue('RU-'.$id)), $qua, array(new Reference($wlmid_refs)));
            $edit_info[] = "[[:d:Property:$P_WLM_ID|".get_wd_property($P_WLM_ID.'')." ($P_WLM_ID)]]: $id";
            $wbFactory->newRevisionSaver()->save($revision, new EditInfo('set '.implode(', ',$edit_info)));
        }

        if ($_REQUEST['action'] == 'export_complex') {
            /* Add new statemenT P361 (part of) identifying complex. If statement exists: warn user, skip action */
            if (empty($row['wv_complex_wdid'])) throw new Exception('Комплекс не задан');
            $q = new ItemId($row['wv_complex_wdid']);
            $p361 = $wd->getByPropertyId($P_PART_OF);
            if (count($p361->toArray())>1) throw new Exception('Неоднозначность — утверждение для P361 уже есть: заменить? добавить? исправить? ...');
            if (!$p361->isEmpty()) foreach ($p361 as $stmt) {
                $wd->removeStatementsWithGuid($stmt->getGuid());
            }
            $wd->addNewStatement(
                new PropertyValueSnak($P_PART_OF, new EntityIdValue($q)),
                null,
                array(new Reference($refSnaks)));
            $wbFactory->newRevisionSaver()->save($revision, new EditInfo("set [[:d:Property:P361|part of (P361)]]: [[:d:$row[wv_complex_wdid]|$row[wv_complex_wdid]]]"));
        }

        if ($_REQUEST['action'] == 'export_egrokn') {
            /* Add new statement P5381 with official cultural heritage identifier (EGROKN). If statement exists: replace it */
            if (empty($wv->knid_new)) throw new Exception('Номер ЕГРОКН не задан');
            // P17 must exists also
            check_and_add_country_statement($id, $wd, $refSnaks, $wv_year);
            set_or_replace_value($wd, new StringValue($wv->knid_new), $P_EGROKN, 
                array(new Reference(array(new PropertyValueSnak($P_STATED_IN, new EntityIdValue(new ItemId(Q_EGROKN)))))));
            $wbFactory->newRevisionSaver()->save($revision, new EditInfo('set '.implode(', ',$edit_info)));
        }

        if ($_REQUEST['action'] == 'export_type') {
            /* Add new statement P31 with typology of cultural heritage monument */
            $p31 = $wd->getByPropertyId($P_INSTANCE_OF);
            if (!$p31->isEmpty()) foreach ($p31 as $stmt) {
                $q = $stmt->getMainSnak()->getDataValue()->getValue()['id'];
                if ($q == Q_RUS_HERITAGE) {
                    $wd->removeStatementsWithGuid($stmt->getGuid());
                }
                if ($q == $WD_TYPOLOGY[$wv->type]) throw new Exception('Тип объекта уже задан');
            }
            $wd->addNewStatement(
                new PropertyValueSnak($P_INSTANCE_OF, new EntityIdValue(new ItemId($WD_TYPOLOGY[$wv->type]))),
                null,
                array(new Reference($refSnaks)));
            $wbFactory->newRevisionSaver()->save($revision, new EditInfo('set [[:d:Property:P31|instance of (P31)]]: [[:d:'.$WD_TYPOLOGY[$wv->type].'|'.$WD_TYPOLOGY[$wv->type].']]'));
        }

        if ($_REQUEST['action'] == 'set_p1435') {
            /* Add new statement P1435 with heritage designation Q8346700 (Q_RUS_HERITAGE) */
            // P17 must exist also
            check_and_add_country_statement($id, $wd, $refSnaks, $wv_year);

            $p1435 = $wd->getByPropertyId($P_HERITAGE);
            $refs = official_acts($wv);
            if (empty($refs) and $wv->knid{2} == '4' and !empty($row['pwd'])) {
                $refs = array($knid_ref);
            }
            // Filter existsing statement
            if (!$p1435->isEmpty()) foreach ($p1435 as $stmt) {
                $value = $stmt->getMainSnak()->getDataValue()->getValue()['id'];
                if (!in_array($value, $WD_PROTECTION)) continue;
                $wd->removeStatementsWithGuid($stmt->getGuid());
            }
            // process dismissed atribute
            $qua = null;
            if (property_exists($wv, 'dismissed')) {
                $snak = null;
                if (preg_match('|do?c?(\d{2})(\d{2})(\d{4})|', $wv->dismissed, $d)) {
                    $snak = new PropertyValueSnak($P_END_TIME,
                        new UnDeserializableValue(array('time'=>"+$d[3]-$d[2]-$d[1]T00:00:00Z", 'timezone'=>0, 'before'=>0, 'after'=>0, 'precision'=>11, 'calendarmodel'=>'http://www.wikidata.org/entity/Q1985727'), 'time', ''));
                } else {
                    $snak = new PropertySomeValueSnak($P_END_TIME);
                }
                $qua = new SnakList(array($snak));
            }
            // Create new statement if missing
            $value_protection = @($WD_PROTECTION[$wv->protection]);
            if (empty($value_protection)) $value_protection = Q_RUS_HERITAGE;
            $wd->addNewStatement(
                new PropertyValueSnak($P_HERITAGE, new EntityIdValue(new ItemId($value_protection))),
                $qua,
                new ReferenceList(array_map(function ($val) {return new Reference($val);}, $refs)));
            $edit_info[] = "[[:d:Property:P1435|heritage designation (P1435)]]: [[:d:$value_protection|$value_protection]]";
            $wbFactory->newRevisionSaver()->save($revision, new EditInfo('set '.implode(', ',$edit_info)));
        }

        if ($_REQUEST['action'] == 'export_municipality') {
            /* Add new statement P131 with municipality. If statement exists: replace it */
            if (empty($wv->munid)) throw new Exception('Для населённого пункта не указана сущность Викиданных');
            // P17 must exist also
            check_and_add_country_statement($id, $wd, $refSnaks, $wv_year);
            set_or_replace_value($wd, new EntityIdValue(new ItemId($wv->munid)), $P_LOC_ATE, array(new Reference($refSnaks)),
                true /*keep qualified statements */);
            $wbFactory->newRevisionSaver()->save($revision, new EditInfo('set '.implode(', ',$edit_info)));
        }

        if ($_REQUEST['action'] == 'export_address') {
            /* Add new statement P2795 with directions[ru]. If statement exists: replace it */
            if (empty($row['wv_address'])) throw new Exception('Не указан адрес');
            // P17 must exist also
            check_and_add_country_statement($id, $wd, $refSnaks, $wv_year);
            set_or_replace_value($wd,
                new UnDeserializableValue(array('language'=>'ru','text'=>$row['wv_address']), 'monolingualtext', ''),
                $P_DIRECTIONS,
                array(new Reference($refSnaks)));
            $wbFactory->newRevisionSaver()->save($revision, new EditInfo('set '.implode(', ',$edit_info)));
        }

        if ($_REQUEST['action'] == 'export_coords') {
            /* Add new statement P625 with coordinates. If statement exists: replace it */
            if (empty($wv->lat) || empty($wv->long)) throw new Exception('Не указаны координаты');
            // P17 must exist also
            check_and_add_country_statement($id, $wd, $refSnaks, $wv_year);
            set_or_replace_value($wd, 
                new UnDeserializableValue(array('latitude'=>(float)$wv->lat,'longitude'=>(float)$wv->long,'altitude'=>null,'precision'=>($wv->precise=='yes'?0.000001:0.001),'globe'=>'http://www.wikidata.org/entity/Q2'), 'globecoordinate', ''),
                $P_COORDINATES,
                array(new Reference($refSnaks)));
            $wbFactory->newRevisionSaver()->save($revision, new EditInfo('set '.implode(', ',$edit_info)));
        }

        if ($_REQUEST['action'] == 'export_ruwiki') {
            /* Set ruwiki link */
            if (empty($wv->wiki)) throw new Exception('Не задана статья Википедии');
            $need_wiki = false;
            if ($wd_item->hasLinkToSite('ruwiki')) {
                if (wiki_strcmp($wd_item->getSiteLink('ruwiki')->getPageName(), $wv->wiki)) $need_wiki = true;
            } else $need_wiki = true;
            if ($need_wiki) {
                $p = get_page_properties(URL_RWP.'w/api.php', $wv->wiki);
                if (!empty($p)) {
                    if (empty($p->properties['wikibase_item'])) {
                        $wd_item->addSiteLink(new \Wikibase\DataModel\SiteLink('ruwiki', $wv->wiki));
                        $wbFactory->newRevisionSaver()->save($revision, new EditInfo('set ruwiki: '.$wv->wiki));
                        purge_page_cache(URL_RWP.'w/api.php', $wv->wiki);
                    } else {
                        error_log($_SESSION['err_msg'] = "wiki '{$wv->wiki}' is already bound to {$p->properties['wikibase_item']}\n");
                    }
                }
            }
        }

        if ($_REQUEST['action'] == 'export_commonscat') {
            /* Set commonswiki link and add P373 statement with Commons category. If non-matching statement exists: replace it */
            if (empty($wv->commonscat)) throw new Exception('Не задана категория Викисклада');
            $need_comcat = false;
            if ($wd_item->hasLinkToSite('commonswiki')) {
                if (wiki_strcmp($wd_item->getSiteLink('commonswiki')->getPageName(), 'Category:'.$wv->commonscat)) $need_comcat = true;
            } else $need_comcat = true;
            if ($need_comcat) {
                $p = get_page_properties(URL_COM.'w/api.php', 'Category:'.$wv->commonscat);
                if (!empty($p)) {
                    if (empty($p->properties['wikibase_item'])) {
                        $wd_item->addSiteLink(new \Wikibase\DataModel\SiteLink('commonswiki', 'Category:'.$wv->commonscat));
                        $edit_info[] = 'commonswiki';
                        purge_page_cache(URL_COM.'w/api.php', 'Category:'.$wv->commonscat);
                    } else {
                        error_log($_SESSION['err_msg'] = "commonscat '{$wv->commonscat}' is already bound to {$p->properties['wikibase_item']}\n");
                    }
                }
            }
            set_or_replace_value($wd, new StringValue($wv->commonscat), $P_COMMONS_CAT, array(new Reference($refSnaks)));
            $wbFactory->newRevisionSaver()->save($revision, new EditInfo('set '.implode(', ',$edit_info)));
        }

        if ($_REQUEST['action'] == 'export_sobory') {
            /* Add new statement P8316 with sobory.ru identifier. If statement exists: replace it */
            if (empty($wv->sobory)) throw new Exception('Значение для sobory не задано');
            set_or_replace_value($wd, new StringValue($wv->sobory), $P_SOBORY,
                array(new Reference(array(new PropertyValueSnak($P_STATED_IN, new EntityIdValue(new ItemId(Q_SOBORY)))))));
            $wbFactory->newRevisionSaver()->save($revision, new EditInfo('set '.implode(', ', $edit_info)));
        }

        if ($_REQUEST['action'] == 'export_temples') {
            /* Add new statement P9343 with temples.ru identifier. If statement exists: replace it */
            if (empty($wv->temples)) throw new Exception('Значение для temples не задано');
            set_or_replace_value($wd, new StringValue($wv->temples), $P_TEMPLES,
                array(new Reference(array(new PropertyValueSnak($P_STATED_IN, new EntityIdValue(new ItemId(Q_TEMPLES)))))));
            $wbFactory->newRevisionSaver()->save($revision, new EditInfo('set '.implode(', ', $edit_info)));
        }

        if ($_REQUEST['action'] == 'export_year') {
            /* Add new statement P571 with year of inception. If statement exists: replace it */
            if ($wv_year<500) throw new Exception('Не могу формализовать дату');
            set_or_replace_value($wd, 
                new UnDeserializableValue(array('time'=>"+$wv_year-00-00T00:00:00Z", 'timezone'=>0, 'before'=>0, 'after'=>0, 'precision'=>9, 'calendarmodel'=>'http://www.wikidata.org/entity/Q1985727'), 'time', ''),
                $P_INCEPTION,
                array(new Reference($refSnaks)));
            $wbFactory->newRevisionSaver()->save($revision, new EditInfo('set '.implode(', ', $edit_info)));
        }

        if ($_REQUEST['action'] == 'export_abolished') {
            /* Add new statement P576 with year of distruction. If statement exists: skip it */
            if (!property_exists($wv, 'status') or $wv->status != 'destroyed') throw new Exception('Нет сведений об утрате');
            $p576 = $wd->getByPropertyId($P_ABOLISHED);
            if (!$p576->isEmpty()) throw new Exception('Сведения об утрате уже внесены');
            $wd->addNewStatement(
                new PropertySomeValueSnak($P_ABOLISHED),
                null,
                array(new Reference($refSnaks)));
            $wbFactory->newRevisionSaver()->save($revision, new EditInfo('set [[:d:Property:P576|abolished... (P576)]]'));
        }

        if ($_REQUEST['action'] == 'create_wd_entity') {
            $itemId = create_wd_entity($row);

            $wv_api = new \Mediawiki\Api\MediawikiApi(URL_RWV.'w/api.php');
            $wv_api->login(new \Mediawiki\Api\ApiUser($AVSBOT['LOGIN'], $AVSBOT['PASSWORD']));
            $res = $wv_api->getRequest(new \Mediawiki\Api\SimpleRequest('query', array('pageids'=>$row['pageid'],'prop'=>'revisions','rvprop'=>'timestamp')));
            $ts = $res['query']['pages'][$row['pageid']]['revisions'][0]['timestamp'];
            $content = file_get_contents(URL_RWV."w/index.php?curid=$row[pageid]&action=raw");
            $new_content = set_wdid($content, $id, $itemId);
            $res = $wv_api->postRequest(new \Mediawiki\Api\SimpleRequest('edit', 
                array('pageid'=>$row['pageid'], 
                      'summary'=>"/* $id */ wdid=$itemId, #CHEΓ", 
                      'basetimestamp'=>$ts, 
                      'token'=>$wv_api->getToken(),
                      'bot'=>true,
                      'text'=>$new_content)));
            $wv->wdid = ''.$itemId;
            $sth = $dbh->prepare("UPDATE monuments SET wdid = ?, json = ? WHERE knid = ?");
            $sth->execute(array(substr($wv->wdid,1), json_encode($wv, JSON_UNESCAPED_UNICODE), $id));
        }

    } catch (Exception $e) {
        $_SESSION['err_msg'] = $e->getMessage();
        error_log($e->getMessage());
        error_log($e->getTraceAsString());
    } finally {
        header("Location: $_SERVER[PHP_SELF]?id=$id");
        exit;
    }
} while (false);

/* Start of interface */
mb_internal_encoding('utf-8');
$footer = '</table></form></body></html>';
echo @("
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv='content-type' content='text/html; charset=utf-8'>
    <script src='https://tools-static.wmflabs.org/cdnjs/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
    <link rel='stylesheet' href='https://unpkg.com/leaflet@1.6.0/dist/leaflet.css' integrity='sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==' crossorigin=''/>
    <script src='https://unpkg.com/leaflet@1.6.0/dist/leaflet.js' integrity='sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==' crossorigin=''></script>
    <!-- links rel='stylesheet' type='text/css' href='//maps.wikimedia.org/leaflet/leaflet.css'/ -->
    <!-- script type='text/javascript' src='//maps.wikimedia.org/leaflet/leaflet.js'></script -->
    <link rel='stylesheet' href='cheg.css' type='text/css'/>
    <title>CHE&Gamma; &mdash; Cultural Heritage Export Gadget".(empty($id)?'':" &mdash; #$id")."</title>
  </head>
  <body>
    <form method='get' autocomplete='off'>
    <table width='90%' align='center'>
    <tr><td><h3>CHE&Gamma; &mdash; Cultural Heritage Export Gadget</h3></td>
    <td align=right>".(is_object($ident) ? 
    "<img src='".URL_WDO."w/skins/Vector/resources/skins.vector.styles/images/user-avatar.svg?b7f58'> <a href='".URL_WDO."wiki:User:{$ident->username}'>{$ident->username}</a>
    &nbsp; <a href='logout.php?id=$id'>Выход</a>"  : '')."&nbsp; <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/Oauth_logo.svg/20px-Oauth_logo.svg.png' border=0 title='OAuth' alt='OAuth logo'>
    <a href='".URL_WDO."wiki/Special:OAuthListConsumers/view/$CRED[TOKEN]'>Инфо</a> &nbsp; ".
    (is_object($ident) ? "<a href='".URL_WDO."wiki/Special:OAuthManageMyGrants'>Права</a> &nbsp;
    <a href='".URL_WDO."w/index.php?title=Special:Contributions/{$ident->username}&tagfilter=OAuth+CID%3A+2189'>Лог</a>" : '')."</td></tr>
    </table>
    <table width='90%' align='center' border><colgroup><col width='19%'/><col width='40%'/><col width='50'/><col width='40%'/></colgroup>
    <tr><td colspan='4' valign='middle'>Введите номер памятника: <input id='id' name='id' value='$id'>
        <input type='hidden' name='csrf' value='$_SESSION[CSRF]'> <input type='submit' value='OK'> &nbsp; <font color='red'>$_SESSION[err_msg]</font></td></tr>");
unset($_SESSION['err_msg']);

if ($id == 0) {
    echo '<tr><td colspan="4"><small>Укажите в поле выше 10-значный номер объекта культурного наследия.</small></td></tr>'.$footer;
    exit;
}

if (empty($row)) {
    echo '<tr><td colspan="4">Ошибка: памятник с таким номером не найден</td></tr>'.$footer;
    exit;
}

echo '
    <tr><td>&nbsp;</td><th><i>Информация из <a href="'.URL_RWV.'wiki/Культурное_наследие_России">Викигида</a></i></th>
        <th>&nbsp;</th><th><i>Информация из <a href="'.URL_WDO.'">Викиданных</a></i></th></tr>';

$button = '&nbsp;';
$WD = null;
if (empty($wv->wdid)) {
    $wikidata = "<font color='red'>НЕ УКАЗАН</font> &nbsp; <button name='action' value='create_wd_entity'><b>Создать сущность Викиданных</b></button>";
} else {
    $WD = get_wd_entity($wv->wdid);
    if (is_object($WD)) {
        $wikidata = "<a href='".URL_WDO."wiki/{$wv->wdid}'>{$wv->wdid}</a>";
    } else {
        $wikidata = "<a href='".URL_WDO."wiki/{$wv->wdid}'><font color='red'>{$wv->wdid}</font></a><br><small>НЕПРАВИЛЬНЫЙ</small>";
    }
}
echo "
    <tr><th class='lc'>Источник</th><td id=source><a href='".URL_RWV."wiki/$row[title]#$id'>$row[title]</a> ".
        (empty($row['pwd']) ? '' : "<small><a href='".URL_WDO."wiki/Q$row[pwd]'>(Q$row[pwd])</a></small>")."</td>
        <td>$button</td><td id=wdid>$wikidata</td></tr>";

$wd_name = '';
$button = '&nbsp;';
if (is_object($WD)) {
    $button = "<button title='Задать label[ru] в Викиданных' name='action' value='export_label'>&rarr;</button>";
    $wd_name = '';
    foreach (get_object_vars($WD->labels) as $l) {
        $wd_name .= "[".$l->language."] ".$l->value."<br>";
        if ($l->language == 'ru' and ($l->value == $wv->name or $l->value == "{$wv->name} ({$wv->municipality})")) $button = '&nbsp;';
    }
}
echo "
    <tr><th class='lc'>Наименование</th><th id=name>".parse_wiki_markup($wv->name)."</th>
        <td>$button</td><td>$wd_name</td></tr>";

$wv_image = get_image($wv->image);
$wd_image = '';
$button = '&nbsp;';
$cnt_img = 0;
if (is_object($WD)) {
    $button = "<button title='Задать ".P_IMAGE." (image) в Викиданных' name='action' value='export_image'>&rarr;</button>";
    if (property_exists($WD->claims, P_IMAGE)) {
        $cnt_img = count($WD->claims->{P_IMAGE});
        $stmt = reset($WD->claims->{P_IMAGE});
        $wd_image = render_wd_statement($stmt, $cv);
        if (!wiki_strcmp($cv, $wv->image)) $button = '&nbsp;';
    } else $wd_image = 'НЕ ЗАДАНО';
    if ($cnt_img>1) $button = '&nbsp;';
}
if (empty($wv->image)) $button = '&nbsp;';
echo "
    <tr><th class='lc'>Изображение</th><td align='center'>".(empty($wv_image) || empty($wv_image['thumb']) ? 'НЕ ЗАДАНО' : "<a href='$wv_image[img_base_url]/wiki/File:".rawurlencode($wv->image)."'><img src='$wv_image[thumb]'><br>{$wv->image}</a>")."
        <td>$button</td><td>$wd_image".
        ($cnt_img > 1 ? "<p align=right><small>Всего $cnt_img фото</small>" : '').'</td></tr>';

$wd_knid = '';
$wv_knid = sprintf($WD_URLS[P_KNID], $id, $id);
if (property_exists($wv, 'uid')) {
    $wv_knid .= "<br>Укр-ID: {$wv->uid}";
}
$button = '&nbsp;';
if (is_object($WD)) {
    $need_button_mask = 0;
    $button = "<button title='Задать ".implode('/',array(P_KNID,P_WLM_ID,P_LIST))." (knid) в Викиданных' name='action' value='export_knid'>&rarr;</button>";
    $indent = '';
    if (property_exists($WD->claims, P_KNID)) {
        foreach ($WD->claims->{P_KNID} as $stmt) {
            $wd_knid .= $indent.render_wd_statement($stmt, $cv);
            $indent = "<br>\n";
            if ($id == $cv) $need_button_mask |= 1;
        }
    } else $wd_knid = 'НЕ ЗАДАН';
    if (property_exists($WD->claims, P_WLM_ID)) {
        $indent = "<br>\n";
        foreach ($WD->claims->{P_WLM_ID} as $stmt) {
            $wd_knid .= $indent.render_wd_statement($stmt, $cv);
            if ($cv == 'RU-'.$id) $need_button_mask |= 2;
        }
    }
    if (property_exists($WD->claims, P_LIST)) {
        $indent = "<br>\n";
        $need_button = true;
        foreach ($WD->claims->{P_LIST} as $stmt) {
            $wd_knid .= $indent.render_wd_statement($stmt, $cv);
            if ($cv == 'Q'.$row['pwd']) $need_button_mask |= 4;
        }
    }
    if ($need_button_mask == 7) $button = '&nbsp;';
}
echo "
    <tr><th class='lc'>Номер в списках</th><td id=knid>$wv_knid</td>
        <td>$button</td><td>$wd_knid</td></tr>";

$wd_complex = '';
$button = '&nbsp;';
$wd_complex_wdid = '-';
if (is_object($WD)) {
    $button = "<button title='Задать ".P_PART_OF." (part of) в Викиданных' name='action' value='export_complex'>&rarr;</button>";
    $indent = '';
    if (property_exists($WD->claims, P_PART_OF)) {
        foreach ($WD->claims->{P_PART_OF} as $stmt) {
            $wd_complex .= $indent.render_wd_statement($stmt, $wd_complex_wdid);
            $indent = "<br>\n";
            if ($wd_complex_wdid == $row['wv_complex_wdid']) $button = '&nbsp;';
        }
    }
}
$wv_complex = '&nbsp;';
if (property_exists($wv, 'complex') and !empty($wv->complex)) {
    $wv_complex = "<a href='".URL_TFO."{$wv->complex}'>{$wv->complex}</a>";
    if ($wv->complex == $wv->knid) $button = '&nbsp;';
} else $button = '&nbsp;';
echo "
    <tr><th class='lc'>Комплекс</th><td>$wv_complex</td>
        <td>$button</td><td>$wd_complex</td></tr>";

$wd_egrokn = '';
$button = '&nbsp;';
if (is_object($WD)) {
    $button = "<button title='Задать ".P_EGROKN." (egrokn) в Викиданных' name='action' value='export_egrokn'>&rarr;</button>";
    $indent = '';
    if (property_exists($WD->claims, P_EGROKN)) {
        foreach ($WD->claims->{P_EGROKN} as $stmt) {
            $wd_egrokn .= $indent.render_wd_statement($stmt, $cv);
            $indent = "<br>\n";
            if ($wv->knid_new == $cv) $button = '&nbsp;';
        }
    } else $wd_egrokn = 'НЕ ЗАДАН';
}
$wv_egrokn = '&nbsp;';
if (property_exists($wv, 'knid_new') and !empty($wv->knid_new))
    $wv_egrokn = sprintf($WD_URLS[P_EGROKN], $wv->knid_new, $wv->knid_new); 
else $button = '&nbsp;';
echo "
    <tr><th class='lc'>Номер <a href='https://opendata.mkrf.ru/opendata/7705851331-egrkn'>ЕГРОКН</a></th><td id=knid_new>$wv_egrokn</td>
        <td>$button</td><td>$wd_egrokn</td></tr>";

$types = array(
    'architecture' => 'градостроительства и архитектуры',
    'history' => 'истории',
    'monument' => 'искусства',
    'archeology' => 'археологии',
    'settlement' => 'Историческое поселение'
);
$wd_type = '';
$button = '&nbsp;';
if (is_object($WD)) {
    $button = "<button title='Задать ".P_INSTANCE_OF." (instance of) в Викиданных' name='action' value='export_type'>&rarr;</button>";
    $indent = '';
    if (property_exists($WD->claims, P_INSTANCE_OF)) {
        foreach ($WD->claims->{P_INSTANCE_OF} as $stmt) {
            $wd_type .= $indent.render_wd_statement($stmt, $cv);
            $indent = "<br>\n";
            if ($WD_TYPOLOGY[$wv->type] == $cv) $button = '&nbsp;';
        }
    } else $wd_type = 'НЕ ЗАДАН';
}
echo @("
    <tr><th class='lc'>Типология</th><td id=type>".($wv->type == 'settlement' ? '' : (substr($id,2,1) == '4' ? 'Объект с признаками памятника ' : 'Памятник '))."{$types[$wv->type]}</td>
        <td>$button</td><td>$wd_type</td></tr>");

$wv_document = '';
if (property_exists($wv, 'document')  and !empty($wv->document)) {
    $wv_document = '<br><small>REF: '.parse_document($wv->document, $wv->region, $d).'</small>';
}
if (property_exists($wv, 'document2') and !empty($wv->document2)) {
    $wv_document .= '<br><small>REF: '.parse_document($wv->document2, $wv->region, $d).'</small>';
}
$wd_status = '';
$button = '&nbsp;';
if (is_object($WD)) {
    $button = "<button title='Обновить ".P_HERITAGE." (heritage designation) в Викиданных и добавить ссылку на документ' name='action' value='set_p1435'>&rarr;</button>"; 
    $indent = '';
    if (property_exists($WD->claims, P_HERITAGE)) {
        foreach ($WD->claims->{P_HERITAGE} as $stmt) {
            $wd_status .= $indent.render_wd_statement($stmt, $cv);
            $indent = "<br>\n";
        }
    } else $wd_status = 'НЕ ЗАДАН';
}
$wv_dismissed = '';
if (property_exists($wv, 'dismissed')) {
    $wv_dismissed = '<br>Снят с охраны<br><small>REF: '.parse_document($wv->dismissed, $wv->region, $d).'</small>';
}
echo @("
    <tr><th class='lc'>Статус</th><td id=protection>{$protection[$wv->protection]}$wv_document$wv_dismissed</td>
        <td>$button</td><td>$wd_status</td></tr>");

$wd_place = '';
$button = '&nbsp;';
$wd_place_wdid = '-';
if (is_object($WD)) {
    $button = "<button title='Задать ".P_LOC_ATE." (located in the ATE) в Викиданных' name='action' value='export_municipality'>&rarr;</button>";
    $indent = '';
    if (property_exists($WD->claims, P_COUNTRY)) {
        foreach ($WD->claims->{P_COUNTRY} as $stmt) {
            $wd_place .= $indent.render_wd_statement($stmt, $cv);
            $indent = "<br>\n";
        }
    } else $wd_place = '<font color=red size=-1>НЕ ЗАДАНА СТРАНА (P17)</font><br>';
    if (property_exists($WD->claims, P_LOC_ATE)) {
        foreach ($WD->claims->{P_LOC_ATE} as $stmt) {
            $wd_place .= $indent.render_wd_statement($stmt, $wd_place_wdid);
            $indent = "<br>\n";
        }
    }
    if (property_exists($WD->claims, P_LOCATION)) {
        foreach ($WD->claims->{P_LOCATION} as $stmt) {
            $wd_place .= $indent.render_wd_statement($stmt, $cv);
            $indent = "<br>\n";
        }
    }
}
$wv_place = 'НЕ ЗАДАН';
if (!empty($wv->municipality)) {
    $wv_place = $wv->municipality;
    if (!empty($wv->munid)) {
        $wv_place .= " ({$wv->munid})";
        $wv_place = "<a href='".URL_WDO."wiki/{$wv->munid}'>$wv_place</a><br>{$regs[$wv->region]} / {$wv->district}";
        if ($wv->munid == $wd_place_wdid) $button = '&nbsp;';
    }
}
if (empty($wv->munid)) $button = '&nbsp;';
echo "
    <tr><th class='lc'>Населённый пункт</th><td id=city>$wv_place</td>
        <td>$button</td><td>$wd_place</td></tr>";

$wd_address = '';
$button = '&nbsp;';
if (is_object($WD)) {
    $button = "<button title='Задать ".P_DIRECTIONS." (directions[ru]) в Викиданных' name='action' value='export_address'>&rarr;</button>";
    $indent = '';
    if (property_exists($WD->claims, P_DIRECTIONS)) {
        foreach ($WD->claims->{P_DIRECTIONS} as $stmt) {
            $wd_address .= $indent.render_wd_statement($stmt, $cv);
            $indent = "<br>\n";
            if ($cv == $row['wv_address']) $button = '&nbsp;';
        }
    }
    $wd_location = '';
    $indent = '';
    foreach (array(P_STREET, P_ADDRESS) as $p) {
        if (property_exists($WD->claims, $p))
        foreach ($WD->claims->{$p} as $stmt) {
            $wd_location .= $indent.render_wd_statement($stmt, $cv);
            $indent = "<br>\n";
        }
    }
    if (!empty($wd_location)) $wd_location = '<small><i>Прочие сведения о расположении:</i></small><br>'.$wd_location;
}
if (empty($row['wv_address'])) $button = '&nbsp;';
echo @("
    <tr><th class='lc'>Адрес</th><td id=address>".(empty($row['wv_address']) ? '&nbsp;' : $row['wv_address'])."</td>
        <td>$button</td><td>$wd_address$wd_location</td></tr>");

$wd_coords = ''; $button = '&nbsp;'; $cv = null; $distance = false;
if (is_object($WD)) {
    $button = "<button title='Задать ".P_COORDINATES." (coordinates) в Викиданных' name='action' value='export_coords'>&rarr;</button>";
    $indent = '';
    if (property_exists($WD->claims, P_COORDINATES)) {
        foreach ($WD->claims->{P_COORDINATES} as $stmt) {
            $wd_coords .= $indent.render_wd_statement($stmt, $cv);
            $indent = "<br>\n";
            if (!empty($wv->lat) and !empty($wv->long)) {
                $distance = ortho_distance($wv->lat, $wv->long, $cv->latitude, $cv->longitude);
                if ($distance < 30 or is_nan($distance)) $button = '&nbsp;';
            }
        }
    }
}
if (empty($wv->lat) || empty($wv->long)) $button = '&nbsp;';
elseif ($distance !== false and !is_nan($distance)) $button .= sprintf('<br>&Delta;=%.0f&nbsp;м', $distance);
echo @("
    <tr><th class='lc'>Координаты</th><td id=coords>".(empty($wv->lat) || empty($wv->long) ? 'НЕ ЗАДАНЫ' : 
        "<div style='margin:2pt;'><a href='https://tools.wmflabs.org/geohack/geohack.php?pagename=".str_replace('/',':',$row['title']).":$id&params={$wv->lat}_N_{$wv->long}_E_globe:earth&language=ru' title='Карты и инструменты GeoHack'>{$wv->lat}, {$wv->long}</a>
        <sup><small><a href='https://maps.yandex.ru/?ll={$wv->long},{$wv->lat}&spn=0.02,0.02&pt={$wv->long},{$wv->lat}&l=map' title='Это место на Яндекс.Картах'>(Я)</a></small></sup>
        <sup><small><a href='https://maps.google.com/maps?ll={$wv->lat},{$wv->long}&spn=0.02,0.02&t=m&q={$wv->lat},{$wv->long}' title='Это место на картах Google'>(G)</a></small></sup>
        <sup><small><a href='https://www.openstreetmap.org/?mlat={$wv->lat}&mlon={$wv->long}&zoom=13&layers=M' title='Это место на карте OpenStreetMap'>(O)</a></small></sup></div>")."
        <td>$button</td><td>".(empty($wd_coords) ? 'НЕ ЗАДАНЫ' : $wd_coords)."
        </td><tr>".(empty($wv->lat) || empty($wv->long) ? '' : "
    <tr><th>&nbsp;</th><td class='lc' colspan=3><div id='mapid'></div>
        <script type='text/javascript'>
            var wvmap = L.map('mapid').setView([{$wv->lat}, {$wv->long}], 13);
            var tile = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
//            var tile = 'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png';
            L.tileLayer(tile, {attribution: '&copy; <a href=\"https://www.openstreetmap.org/\">OpenStreetMap</a> contributors, <a href=\"https://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>'}).addTo(wvmap);
            var wv_marker = L.marker([{$wv->lat}, {$wv->long}],{title:'Wikivoyage'}).addTo(wvmap);
            wv_marker.bindPopup('Wikivoyage');
            L.Icon.Default.prototype.options.imagePath = 'https://ru_monuments.toolforge.org/snow/';
            ".(is_object($cv) ? "var wd_marker = L.marker([{$cv->latitude}, {$cv->longitude}],{title:'Wikidata'}).addTo(wvmap);
            wd_marker.bindPopup('Wikidata');" : '')."
        </script></td></tr>"));

$wd_wiki = '&nbsp;';
$button = '&nbsp;';
if (is_object($WD) and property_exists($WD->sitelinks, 'ruwiki')) {
    $wd_wiki = sprintf($WD_URLS['sitelinks'], $WD->sitelinks->ruwiki->url, $WD->sitelinks->ruwiki->title);
    if (wiki_strcmp($WD->sitelinks->ruwiki->title, $wv->wiki))
        $button = "<button title='Задать sitelinks[ruwiki] в Викиданных' name='action' value='export_ruwiki'>&rarr;</button>";
}
if (empty($wv->wiki)) $button = '&nbsp;';
echo @("
    <tr><th class='lc'>Статья в РуВики</th><td id=article>".(empty($wv->wiki) ? '&nbsp;' : "<a href='https://ru.wikipedia.org/wiki/{$wv->wiki}'>{$wv->wiki}")."</td>
        <td>$button</td><td>$wd_wiki</td></tr>");

$wd_com = '&nbsp;';
$button = '&nbsp;';
$indent = '';
if (is_object($WD)) {
    if (property_exists($WD->sitelinks, 'commonswiki')) {
        $wd_com = sprintf($WD_URLS['sitelinks'], $WD->sitelinks->commonswiki->url, $WD->sitelinks->commonswiki->title);
        $indent = "<br>\n";
    }
    $button =  "<button title='Задать sitelinks[commonswiki] и ".P_COMMONS_CAT." в Викиданных' name='action' value='export_commonscat'>&rarr;</button>";
    if (property_exists($WD->claims, P_COMMONS_CAT)) {
        foreach ($WD->claims->{P_COMMONS_CAT} as $stmt) {
            $wd_com .= $indent.render_wd_statement($stmt, $cv);
            $indent = "<br>\n";
            if (!wiki_strcmp($cv, $wv->commonscat)) $button = '&nbsp;';
        }
    }
}
if (empty($wv->commonscat)) $button = '&nbsp;';
echo @("
    <tr><th class='lc'>Викисклад</th><td id=commons>".(empty($wv->commonscat) ? '&nbsp;' : sprintf($WD_URLS[P_COMMONS_CAT], $wv->commonscat, $wv->commonscat))."</td>
        <td>$button</td><td>$wd_com</td></tr>");

$wv_sobory = @($wv->sobory);
$wd_sobory = '';
$button = '&nbsp;';
if (is_object($WD)) {
    $button = "<button title='Задать ".P_SOBORY." (sobory ID) в Викиданных' name='action' value='export_sobory'>&rarr;</button>";
    $indent = '';
    if (property_exists($WD->claims, P_SOBORY)) {
        foreach ($WD->claims->{P_SOBORY} as $stmt) {
            $wd_sobory .= $indent.render_wd_statement($stmt, $cv);
            $indent = "<br>\n";
            if ($cv == $wv_sobory) $button = '&nbsp;';
        }
    }
    if (empty($wv_sobory)) $button = '&nbsp;';
}
if (!empty($wv_sobory)) $wv_sobory = sprintf($WD_URLS[P_SOBORY], $wv_sobory, $wv_sobory);
if (!empty($wv_sobory) or !empty($wd_sobory)) {
    echo @("
    <tr><th class='lc'>Номер в <a href='https://sobory.ru'>Народном каталоге православной архитектуры</a></th>
    <td id=sobory>$wv_sobory</td><td>$button</td><td>$wd_sobory</td></tr>");
}

$wv_temples = @($wv->temples);
$wd_temples = '';
$button = '&nbsp;';
if (is_object($WD)) {
    $button = "<button title='Задать ".P_TEMPLES." (temples ID) в Викиданных' name='action' value='export_temples'>&rarr;</button>";
    $indent = '';
    if (property_exists($WD->claims, P_TEMPLES)) {
        foreach ($WD->claims->{P_TEMPLES} as $stmt) {
            $wd_temples .= $indent.render_wd_statement($stmt, $cv);
            $indent = "<br>\n";
            if ($cv == $wv_temples) $button = '&nbsp;';
        }
    }
    if (empty($wv_temples)) $button = '&nbsp;';
}
if (!empty($wv_temples)) $wv_temples = sprintf($WD_URLS[P_TEMPLES], $wv_temples, $wv_temples);
if (!empty($wv_temples) or !empty($wd_temples)) {
    echo @("
    <tr><th class='lc'>Номер в проекте <a href='http://temples.ru'>Храмы России</a></th>
    <td id=temples>$wv_temples</td><td>$button</td><td>$wd_temples</td></tr>");
}

$wv_year = @($wv->year+0);
if ($wv_year<=100) $wv_year=0;
$wd_year = '';
$button = '&nbsp;';
if (is_object($WD)) {
    $button = "<button title='Задать ".P_INCEPTION." (inception) — $wv_year' name='action' value='export_year'>=$wv_year</button>";
    $indent = '';
    if (property_exists($WD->claims, P_INCEPTION)) {
        foreach ($WD->claims->{P_INCEPTION} as $stmt) {
            $wd_year .= $indent.render_wd_statement($stmt, $cv);
            $indent = "<br>\n";
            if ($cv == $wv_year) $button = '&nbsp;';
        }
    }
    if (empty($wv_year)) $button = '&nbsp;';
}
echo @("
    <tr><th class='lc'>Время создания/возникновения</th>
    <td id=sobory>{$wv->year}</td><td>$button</td><td>$wd_year</td></tr>");

if (property_exists($wv, 'status') and $wv->status == 'destroyed') {
    $wd_destroyed = ''; 
    $button = '&nbsp;';
    if (is_object($WD)) {
        $button = "<button title='Здадать ".P_ABOLISHED." (abolished)' name='action' value='export_abolished'>&rarr;</a>";
        $indent = '';
        if (property_exists($WD->claims, P_ABOLISHED)) {
            $button = '&nbsp;';
            foreach ($WD->claims->{P_ABOLISHED} as $stmt) {
                $wd_destroyed .= $indent.render_wd_statement($stmt, $cv);
                $indent = "<br>\n";
            }
        }
    } 
    echo @("
    <tr><th class='lc'>Статус утраты</th>
    <td id=sobory>Объект утрачен</td><td>$button</td><td>$wd_destroyed</td></tr>");
}

$wd_other = '';
if (is_object($WD)) {
    $indent = '';
    foreach (array(P_ABOLISHED, P_ARCHITECT, P_CREATOR, P_STYLE, P_COMMEMORATES, P_DEPICTS, P_OCCUPANT) as $p) {
        if ($p == P_ABOLISHED and property_exists($wv, 'status') and $wv->status == 'destroyed') continue;
        if (property_exists($WD->claims, $p))
            foreach ($WD->claims->{$p} as $stmt) {
                $wd_other .= $indent.render_wd_statement($stmt, $cv);
                $indent = "<br>\n";
            }
    }
}
echo @("
    <tr><th class='lc'>Прочие сведения</th><td id=description>".(empty($wv->author) ? '' : $wv->author.($wv->author{-1} == '.' ? '' : '.')).
    ' '.parse_wiki_markup($wv->description)."</td><td></td><td>$wd_other</td></tr>");

echo $footer;

/* Some fields may contain wiki links — parse it */
function parse_wiki_markup($str) {
    if (!empty($str)) {
        // process wiki-links [[title|description]]
        $str = preg_replace_callback(
            '!\[{2}([^|\]]*)\|?([^\]]*)\]{2}!u',
            function ($matches) {return "<a href=\"".URL_RWV."wiki/$matches[1]\">".(empty($matches[2]) ? $matches[1] : $matches[2]).'</a>';},
            $str);
        // process external links [url description]
        $str = preg_replace_callback(
            '!\[([hH][tT][tT][pP][sS]?://[^\][:space:]]*)\s*([^\]]*)\]!u',
            function ($matches) {return "<a href=\"$matches[1]\">".(empty($matches[2]) ? $matches[1] : $matches[2]).'</a>';},
            $str);
    }
    return $str;
}

/* Compare two probably non-normalized wiki links */
function wiki_strcmp($str1, $str2) {
    $str1 = str_replace('_', ' ', $str1);
    $str2 = str_replace('_', ' ', $str2);
    return strcmp($str1, $str2);
}
